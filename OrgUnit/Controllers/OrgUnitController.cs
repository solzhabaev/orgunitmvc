﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrgUnit.Controllers
{
    using Repositories;
    using Models;
    using ViewModels;
    using System.Web.Mvc;
    public class OrgUnitController : Controller
    {
        private OrgUnitRepo _repo;

        public OrgUnitController()
        {
            _repo = new OrgUnitRepo();
        }

        [HttpPost]
        public ActionResult Create(OrgUnitCreateVM orgUnitCreateInfo)
        {
            var data = _repo.Create(orgUnitCreateInfo.title, orgUnitCreateInfo.parrentOrgUnitId);

            var res = ConvertFromModel(data);

            return Json(res);
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        [HttpPost]
        public ActionResult AddHeader(OrgUnitAddHeaderVM orgUnitAddHeaderInfo)
        {
            var data = _repo.AddHeader(orgUnitAddHeaderInfo.orgUnitId, orgUnitAddHeaderInfo.userId);

            var res = ConvertFromModel(data);

            return Json(res);
        }

        [HttpPost]
        public ActionResult AddEmployee(OrgUnitAddEmployeeVM orgUnitAddEmployeeInfo)
        {
            var data = _repo.AddEmployee(orgUnitAddEmployeeInfo.orgUnitId, orgUnitAddEmployeeInfo.userId);

            var res = ConvertFromModel(data);

            return Json(res);
        }

        [HttpDelete]
        public ActionResult RemoveEmployee(OrgUnitRemoveEmployeeVM orgUnitRemoveEmployeeInfo)
        {
            var data = _repo.RemoveEmployee(orgUnitRemoveEmployeeInfo.orgUnitId, orgUnitRemoveEmployeeInfo.userId);

            var res = ConvertFromModel(data);

            return Json(res);
        }

        [HttpGet]
        public ActionResult GetDetail(int id)
        {
            var data = _repo.GetOrderDetail(id);

            var res = ConvertFromModel(data);

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMainOrgUnits()
        {
            var orgUnits = _repo.GetMainOrgUnits();
            var res = new List<OrgUnitVM>();

            foreach (var item in orgUnits)
            {
                res.Add(ConvertFromModel(item));
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllOrgUnits()
        {
            var orgUnits = _repo.GetAllOrgUnits();
            var res = new List<OrgUnitVM>();

            foreach (var item in orgUnits)
            {
                var val = new OrgUnitVM()
                {
                    id= item.OrgUnitId,
                    title = item.Title,
                    headUser = new UserVM()
                    {
                        id = item.HeadUser.UserId,
                        login = item.HeadUser.Login,
                        password = item.HeadUser.Password
                    },
                    orgUnitParrent = new OrgUnitVM()
                    {
                        id = item.OrgUnitParrent.OrgUnitId,
                        title = item.OrgUnitParrent.Title
                    },
                };

                var empls = new List<UserVM>();

                foreach (var user in item.Employees)
                {
                    empls.Add(new UserVM()
                    {
                        id = user.UserId,
                        login = user.Login,
                        password = user.Password
                    });
                }

                val.employees = empls;

                res.Add(val);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        private OrgUnitVM ConvertFromModel(OrgUnit unit)
        {
            var res = new OrgUnitVM();

            if (unit == null)
            {
                return null;
            }

            res.id = unit.OrgUnitId;
            res.title = unit.Title;

            if (unit.OrgUnitParrent != null)
            {
                res.orgUnitParrent = new OrgUnitVM()
                {
                    id = unit.OrgUnitParrent.OrgUnitId,
                    title = unit.OrgUnitParrent.Title
                };
            }
            
            var unitChilds = new List<OrgUnitVM>();
            if (unit.OrgUnitChilds != null)
            {
                foreach (var item in unit.OrgUnitChilds)
                {
                    unitChilds.Add(ConvertFromModel(item));
                }
                res.orgUnitChilds = unitChilds;
            }

            res.headUser = unit.HeadUser == null ? null : ConvertFromModel(unit.HeadUser);
            var users = new List<UserVM>();

            if (unit.Employees != null)
            {
                foreach (var item in unit.Employees)
                {
                    users.Add(ConvertFromModel(item));
                }
                res.employees = users;
            }
            
            return res;
        }

        private UserVM ConvertFromModel(User user)
        {
            var res = new UserVM();

            res.id = user.UserId;
            res.login = user.Login;
            res.password = user.Password;

            return res;
        }

    }
}
