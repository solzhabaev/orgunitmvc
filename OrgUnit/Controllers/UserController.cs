﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrgUnit.Controllers
{
    using Repositories;
    using Models;
    using ViewModels;
    using System.Web.Mvc;
    public class UserController : Controller
    {
        private UserRepo _repo;

        public UserController()
        {
            _repo = new UserRepo();
        }

        [HttpPost]
        public ActionResult Login(UserLoginInputVM loginInfo)
        {
            var user = _repo.Login(loginInfo.login, loginInfo.password);

            UserVM res = null;

            if (user != null)
            {
                res = new UserVM()
                {
                    id = user.UserId,
                    login = user.Login,
                    password = user.Password
                };
            }
            
            return Json(res);
        }

        [HttpPost]
        public ActionResult Register(UserLoginInputVM loginInfo)
        {
            var user = _repo.Register(loginInfo.login, loginInfo.password);

            var res = new UserVM()
            {
                id = user.UserId,
                login = user.Login,
                password = user.Password
            };

            return Json(res);
        }

        [HttpGet]
        public ActionResult Employees()
        {
            var users = _repo.GetUsers();
            var res = new List<UserVM>();

            foreach (var item in users)
            {
                res.Add(new UserVM()
                {
                    id = item.UserId,
                    login = item.Login,
                    password = item.Password
                });
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}
