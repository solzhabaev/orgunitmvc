namespace OrgUnit.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;

    public partial class Model1 : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<OrgUnit> OrgUnits { get; set; }

        public Model1()
            : base("name=Model1")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }

    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        // navigation property
        public virtual OrgUnit OrgUnit { get; set; }
    }

    public class OrgUnit
    {
        [Key]
        public int OrgUnitId { get; set; }
        public string Title { get; set; }

        // navigation property
        public virtual OrgUnit OrgUnitParrent { get; set; }

        // navigation property
        public virtual List<OrgUnit> OrgUnitChilds { get; set; }

        // navigation property
        public virtual User HeadUser { get; set; }

        // navigation property
        public virtual List<User> Employees { get; set; }

    }

}
