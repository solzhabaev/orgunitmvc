﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrgUnit.Repositories
{
    using Models;
    using System.Data.Entity;
    class OrgUnitRepo
    {
        private Model1 _context;

        public OrgUnitRepo()
        {
            _context = new Model1();
        }

        public OrgUnit Create(string title, int? parrentOrgUnitId)
        {
            var maxId = 1;

            if (_context.OrgUnits.Any())
            {
                maxId = _context.OrgUnits.Max(x => x.OrgUnitId) + 1;
            }

            var orgUnit = new OrgUnit()
            {
                OrgUnitId = maxId,
                Title = title,
            };

            if (parrentOrgUnitId.HasValue)
            {
                var parrentOrgUnit = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == parrentOrgUnitId.Value);
                orgUnit.OrgUnitParrent = parrentOrgUnit;
            }

            _context.OrgUnits.Add(orgUnit);

            _context.SaveChanges();
            return orgUnit;
        }
        
        public void Delete(int orgUnitId)
        {
            var orgUnit = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);

            foreach (var item in orgUnit.Employees)
            {
                orgUnit.Employees.Remove(item);
            }

            ClearCollection(orgUnit.OrgUnitChilds);

            _context.OrgUnits.Remove(orgUnit);
            _context.SaveChanges();
        }

        private void ClearCollection(List<OrgUnit> collection)
        {


            foreach (var item in collection)
            {
                if (item.OrgUnitChilds.Any())
                {
                    ClearCollection(item.OrgUnitChilds);
                }
            }

            collection.RemoveRange(0, collection.Count);
        }


        public OrgUnit AddHeader(int orgUnitId, int userId)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserId == userId);
            var orgUnit = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);

            orgUnit.HeadUser = user;
            _context.SaveChanges();

            return orgUnit;
        }

        public OrgUnit AddEmployee(int orgUnitId, int userId)
        {
            var orgUnit = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);
            var user = _context.Users.FirstOrDefault(x => x.UserId == userId);

            orgUnit.Employees.Add(user);
            user.OrgUnit = orgUnit;
            _context.SaveChanges();

            var res = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);

            return res;
        }

        public OrgUnit RemoveEmployee(int orgUnitId, int userId)
        {
            var orgUnit = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);
            var user = _context.Users.FirstOrDefault(x => x.UserId == userId);

            if (orgUnit.Employees.Any(x => x.UserId == user.UserId))
            {
                orgUnit.Employees.Remove(user);
            }

            user.OrgUnit = null;
            _context.SaveChanges();

            var res = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);

            return res;
        }

        public OrgUnit GetOrderDetail(int orgUnitId)
        {
            var res = _context.OrgUnits.FirstOrDefault(x => x.OrgUnitId == orgUnitId);

            return res;
        }

        public List<OrgUnit> GetMainOrgUnits()
        {
            var data = _context.OrgUnits.Where(x => x.OrgUnitParrent == null).Include("Employees").Include("OrgUnitChilds").ToList();
            return data;
        }

        public List<OrgUnit> GetAllOrgUnits()
        {
            return _context.OrgUnits.ToList();
        }

    }
}