﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrgUnit.Repositories
{
    using Models;

    class UserRepo
    {
        private Model1 _context;

        public UserRepo()
        {
            _context = new Model1();
        }

        public User Login(string login, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.Login == login && x.Password == password);

            return user;
        }

        public User Register(string login, string password)
        {
            if (_context.Users.Any(x => x.Login == login))
            {
                return null;
            }

            var maxId = 1;

            if (_context.Users.Any())
            {
                maxId = _context.Users.Max(x => x.UserId) + 1;
            }

            var user = new User()
            {
                UserId = maxId,
                Login = login,
                Password = password
            };

            var res = _context.Users.Add(user);
            _context.SaveChanges();

            return res;
        }

        public List<User> GetUsers()
        {
            var users = _context.Users.ToList();
            return users;
        }

    }
}